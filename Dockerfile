FROM oven/bun:latest
WORKDIR /usr/src/app

COPY package.json ./
COPY bun.lockb ./
COPY src ./src

RUN bun install

# run the app
USER bun
EXPOSE 3000/tcp
ENTRYPOINT [ "bun", "run", "src/index.ts" ]
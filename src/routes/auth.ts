import { Hono } from "hono";
import { validAuth } from "../validation/auth";
import { db } from "../db";

const Auth = new Hono();

export const routerAuth = Auth
  // Логин
  .post("/login", validAuth(), async ({ req, jsonT, get }) => {
    const { username, password } = req.valid("form");
    await db.user.findFirst({ where: {} });
    const payload = get("jwtPayload");
    return jsonT(payload);
  });

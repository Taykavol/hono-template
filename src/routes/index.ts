import { Hono } from "hono";
import { routerHello } from "../routes/hello";
import { routerAuth } from "./auth";

export const app = new Hono();
export const router = app
  .route("/hello", routerHello)
  .route("/auth", routerAuth)
  .get("/health", async () => {
    return new Response("OK", { status: 200 });
  });

import { Hono } from "hono";
import { validHello } from "../validation/hello";
import { jwt } from "hono/jwt";
import { Prisma, User } from "@prisma/client";
import { getPayload } from "../utlis/getPayload";
import { env } from "process";

const hello = new Hono();

hello.use("*", jwt({ secret: Bun.env.JWT_SECRET }));

export const routerHello = hello
  // Get Hello by id
  .get("/:id", ({ req, jsonT, get }) => {
    const payload = getPayload(get);
    return jsonT({ id: req.param("id") });
  })

  // Create Hello
  .post("/", validHello(), async ({ req, jsonT, get }) => {
    const { body } = req.valid("form");
    const payload = get("jwtPayload");
    return jsonT(payload);
  });

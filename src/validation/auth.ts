import { zValidator } from "@hono/zod-validator";
import { z } from "zod";

export const validAuth = () =>
  zValidator(
    "form",
    z.object({
      username: z.string(),
      password: z
        .string()
        .min(6)
        .max(10)
        .regex(/^[a-zA-Z0-9]+$/),
    })
  );

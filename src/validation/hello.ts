import { zValidator } from "@hono/zod-validator";
import { z } from "zod";

export const validHello = () =>
  zValidator(
    "form",
    z.object({
      body: z.string(),
      simple: z.string(),
    })
  );

type Payload = {
  id: number;
  username: string;
};

export const getPayload = (get: any) => {
  let payload = get("jwtPayload");
  return payload as Payload;
};

import { app, router } from "./routes";

// Импорт типа приложения для rpc
export type AppType = typeof router;

// Запуск приложения
export default app;

declare module "bun" {
  interface Env {
    JWT_SECRET: string;
  }
}

import { AppType } from "./src/index";
import { hc } from "hono/client";
import { decode, sign, verify } from "hono/jwt";

const payload = {
  sub: "user123",
  role: "admin",
};
const secret = "secret";
sign(payload, secret).then((token) => {
  const client = hc<AppType>("http://localhost:3000/", {
    headers: {
      Authorization: "Bearer " + token,
    },
  });
  client.hello[":id"].$get({ param: { id: "hono" } }).then(async (res) => {
    console.log(res);
    if (res.ok) {
      const data = await res.json();
      console.log(data);
    } else {
      console.log(res.status);
    }
  });
  client.index.$get().then(async (res) => {
    console.log(res);
    if (res.ok) {
      const data = await res.json();
      console.log(data);
    }
  });
});

// async function hello_world() {
//   const resp = await client.hello[":id"].$get({ param: { id: "hono" } });
// }

// client.hello
//   .$post({ form: { body: "hono", simple: "hono" } })
//   .then(async (res) => {
//     console.log(res);
//     try {
//       const data = await res.json();
//       console.log(data);
//     } catch (e) {
//       console.log(e);
//     }
//   });

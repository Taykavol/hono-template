### ENV

Добавьте env файлы.
JWT_SECRET
DATABASE_URL

### Docker

docker build -f ./Dockerfile-dev -t template-hono-dev .
docker run -p 4001:3000 --name template-homo-dev template-hono-dev
docker rm -f template-homo-dev

docker build -t template-hono .
docker run -p 4001:3000 template-hono
docker rm -f template-homo

<!-- docker run --restart always --name hono-postgres -d postgres -->

### Postgres

docker run --restart always --name hono-postgres -e POSTGRES_USER=hono -e POSTGRES_PASSWORD=hono -p 5431:5432 -e POSTGRES_DB=hono -d postgres
npx prisma db push
